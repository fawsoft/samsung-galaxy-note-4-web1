( function( $ ) {

	$(document).scrollTo(0);
	var wasAnimated = false;
	
	if($(document).width() >= 1200){
		$(document).on('mousewheel', function(event, delta){ 
			if(!wasAnimated){
				if(delta < 0 && $(document).scrollTop() < 50){
					$('#main-photo').animate({width:$('#main-photo').data('width')+'px'});
					$('#main-text').animate({top:$('#main-text').data('top')+'px'});
					$('.kafel-3').animate({marginTop:'310px'});
					event.preventDefault();
					wasAnimated = true;
				}
			} 
			handleScrolls();
		});
		
		$(document).on('scroll', function(event){ 
			if(!wasAnimated){ 
				$('#main-photo').animate({width:$('#main-photo').data('width')+'px'});
				$('#main-text').animate({top:$('#main-text').data('top')+'px'});
				$('.kafel-3').animate({marginTop:'310px'});
				event.preventDefault();
				wasAnimated = true;
			} 
			handleScrolls();
		});
	}
} )( jQuery );

function handleScrolls(){ 

	if($(document).scrollTop() < 800){ 
		$('.main-photo').show();
		$('.endo').hide();
		$('.funindesign').hide();
		$('.student').hide(); 
		$('.kafel-1').css({height:'232px'});
	}
	
	if( $(document).scrollTop() >= 800 && $(document).scrollTop() < 1500){ 
		$('.main-photo').hide();
		$('.endo').show();
		$('.funindesign').hide();
		$('.student').hide();  
		$('.kafel-1').css({height:'216px'});
	}
	
	if($(document).scrollTop() >= 1500 && $(document).scrollTop() < 2300){ 
		$('.main-photo').hide();
		$('.endo').hide(); 
		$('.funindesign').show();
		$('.student').hide(); 
		$('.kafel-1').css({height:'216px'});
	}
	
	if($(document).scrollTop() >= 2300){ 
		$('.main-photo').hide();
		$('.endo').hide();
		$('.funindesign').hide(); 
		$('.student').show(); 
		$('.kafel-1').css({height:'216px'});
	}

}