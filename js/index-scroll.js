( function( $ ) {

	$(document).scrollTo(0);
	var wasAnimated = false;
	
	if($(document).width() > 1200){
		$(document).on('mousewheel', function(event, delta){ 
			if(!wasAnimated){
				if(delta < 0 && $(document).scrollTop() < 50){
					$('#main-photo').animate({width:$('#main-photo').data('width')+'px'});
					$('#main-text').animate({top:$('#main-text').data('top')+'px'});
					$('.kafel-2').animate({marginTop:'380px'});
					event.preventDefault();
					wasAnimated = true;
				}
			} 
			handleScrolls();
		});
		
		$(document).on('scroll', function(event){ 
			if(!wasAnimated){ 
				$('#main-photo').animate({width:$('#main-photo').data('width')+'px'});
				$('#main-text').animate({top:$('#main-text').data('top')+'px'});
				$('.kafel-2').animate({marginTop:'380px'});
				event.preventDefault();
				wasAnimated = true;
			} 
			handleScrolls();
		});
	}
} )( jQuery );

function handleScrolls(){ 

	if($(document).scrollTop() >= 600){
		$('.tlo').css({background: '#DEE8EA'});
		$('.tlo-kafel-1').css({background: '#DEE8EA'});
	}else{
		$('.tlo').css({background: '#DEE8EA url(images/tlo.png) center top no-repeat', backgroundAttachment: 'fixed'});
		$('.tlo-kafel-1').css({background: '#DEE8EA url(images/tlo.png) center top no-repeat', backgroundAttachment: 'fixed'});
	}

	if($(document).scrollTop() < 750){ 
		$('.main-photo').show();
		$('.brzoska').hide(); 
		$('.endo').hide();
		$('.funindesign').hide();
		$('.student').hide(); 
		$('.kafel-1').css({height:'234px'});
	}
	
	if($(document).scrollTop() >= 750 && $(document).scrollTop() < 1600){ 
		$('.main-photo').hide();
		$('.brzoska').show(); 
		$('.endo').hide();
		$('.funindesign').hide();
		$('.student').hide();  
		$('.kafel-1').css({height:'214px'});
	}
	
	if($(document).scrollTop() >= 1600 && $(document).scrollTop() < 2400){ 
		$('.main-photo').hide();
		$('.brzoska').hide();
		$('.endo').show(); 
		$('.funindesign').hide();
		$('.student').hide(); 
		$('.kafel-1').css({height:'214px'});
	}
	
	if($(document).scrollTop() >= 2400 && $(document).scrollTop() < 3200){ 
		$('.main-photo').hide();
		$('.brzoska').hide();
		$('.endo').hide();
		$('.funindesign').show(); 
		$('.student').hide(); 
		$('.kafel-1').css({height:'215px'});
	}
	
	if($(document).scrollTop() >= 3200){ 
		$('.main-photo').hide();
		$('.brzoska').hide();
		$('.endo').hide();
		$('.funindesign').hide();
		$('.student').show(); 
		$('.kafel-1').css({height:'216px'});
	}
}