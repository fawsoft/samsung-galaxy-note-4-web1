( function( $ ) {
	
	//$('body').click(function(){alert($(document).width());});
	
	$('.design-select').click(function(){
		$('.design-select').removeClass('selected');
		$(this).addClass('selected');
		$('#design-big').attr('src', 'images/'+$(this).data('design')+'.png');
		$('#design-small').attr('src', 'images/'+$(this).data('design')+'-small.png');
	});
	
	$('.run-video').click(function(){
		$('#video-iframe').attr('src', $(this).data('video'));
	});
	
	$('#video-left').click(function(){
		var screenWidth = $(document).width(); 
		var currVideo = $('#video-slider .selected');
		var prevVideo = currVideo.parent().prev().find('img');
		if(prevVideo.length){
			$('#video-iframe').attr('src', prevVideo.data('video'));
			$('.run-video').removeClass('selected');
			prevVideo.addClass('selected');
		} 
	});
	
	$('#video-right').click(function(){
		var screenWidth = $(document).width();
		var amount = $('#video-slider li').length;
		var left = parseInt($('#video-slider').css('left').replace(/px/, '')); 
		var currVideo = $('#video-slider .selected');
		var nextVideo = currVideo.parent().next().find('img');
		if(nextVideo.length){
			$('#video-iframe').attr('src', nextVideo.data('video'));
			$('.run-video').removeClass('selected');
			nextVideo.addClass('selected');
		} 
	});
	
	$('#scrollToKamera').click(function(){
		$('body').scrollTo('.kamera', 800);
	});
	
	$('#scrollToWybor').click(function(){
		$('body').scrollTo('.wybor', 800, {offset:-25});
	});

	$('#scrollToPhotoNote').click(function(){
		$('body').scrollTo('.photo-note', 800);
	});
	
	$('#scrollToDlugopis').click(function(){
		$('body').scrollTo('.dlugopis', 800, {offset:-25});
	});
	
	$('#scrollToMozliwosci').click(function(){
		$('body').scrollTo('.mozliwosci', 800);
	});
	
	$('#scrollToBateria').click(function(){
		$('body').scrollTo('.bateria', 800, {offset:-25});
	});
	
	$('.burger-menu').click(function(){
		$('#burger').toggle();
	});
	
	$('.ico').click(function(event){
		event.preventDefault();
		$('#dialog-img').attr('src', 'images/'+$(this).data('img')+'.png');
		$('#dialog-title').html($(this).data('title'));
		$('#dialog-headline').html($(this).data('headline'));
		$('#dialog-desc').html($(this).data('desc'));
		$('#dialog').dialog({
			modal: true
		});
	});
	
	$('.dialog-close').click(function(){
		$('#dialog').dialog('close');
	});
	
	$('#design-down').click(function(){
		$('.design-select.selected').next().click();
		if($('.design-select.selected').next().length==0){
			$('#arrow-down').hide();
			$('#arrow-next').show();
		}
	});
	
	$('.movie').click(function(event){
		event.preventDefault();
		$('#video-iframe').attr('src', $(this).data('video'));
		$('#video-popup').dialog({
			modal: true
		});
	});
	
	$('.video-dialog-close').click(function(){
		$('#video-iframe').attr('src', '');
		$('#video-popup').dialog('close');
	});
	
} )( jQuery );