( function( $ ) {
	
	var deltaAmounts = 0;
	
	$(document).on('mousewheel', function(event, delta){
		
		if(Math.abs(delta)==1){
			deltaAmounts += 25;
		}else{
			deltaAmounts += 1;
		}

		if(deltaAmounts >= 100){
			if(delta < 0){
				$('.design-select.selected').next().click();
			}else if(delta > 0 && $(document).scrollTop() == 0){
				$('.design-select.selected').prev().click();
			}
			deltaAmounts = 0;
		}
		if(delta < 0 && $('.design-select.selected').next().length>0){
			event.preventDefault();
		}else{
			$('#arrow-down').hide();
			$('#arrow-next').show();
		}

		if(delta > 0){
			$('#arrow-down').show();
			$('#arrow-next').hide();
		}
	});
	
} )( jQuery );